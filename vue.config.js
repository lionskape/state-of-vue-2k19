// vue.config.js
const path = require("path");
const merge = require("webpack-merge");

module.exports = {
  chainWebpack: config => {
    config.module
      .rule("vue")
      .use("vue-loader")
      .tap(options =>
        merge(options, {
          loaders: {
            scss: [
              {
                loader: "sass-loader",
                options: {
                  includePaths: [path.resolve(__dirname, "./node_modules")]
                }
              }
            ]
          }
        })
      );
  }
};
