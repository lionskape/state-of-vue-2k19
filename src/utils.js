const isMobile = () => matchMedia("(max-width: 767px)").matches;
const isPdf = () => window.location.search.match(/print-pdf/gi);

export { isMobile, isPdf };
